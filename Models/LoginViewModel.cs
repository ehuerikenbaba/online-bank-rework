﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Onlinebank_rework.Models
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Username field required")]
        public string Username { get; set; }

        [Required (ErrorMessage = "Password field required")]
        public string password { get; set; }
    }
}
