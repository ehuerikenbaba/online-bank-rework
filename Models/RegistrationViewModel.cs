﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Onlinebank_rework.Models
{
    public class RegistrationViewModel
    {
        [Required(ErrorMessage = "Full Name field required")]
        public string  FullName { get; set; }

        [Required(ErrorMessage = "UserName field required")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Email field required")]
        public string Email { get; set; }

        [Required(ErrorMessage = "password field required")]
        public string Password { get; set; }

    }
}
